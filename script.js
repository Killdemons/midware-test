function show() {
  var progressBar = document.getElementById('progressBar');
  var quantity = document.getElementsByName('quantity')[0].value;
  var goal = document.getElementsByName('goal')[0].value;
  width = (quantity*100)/goal;
  if (width <= 100) {
    progressBar.style.width = width + '%';
    progressBar.innerHTML = width * 1  + '%';
  }
}